const GETRAENKE_ALK = "Getränke (alkoholisch)";
const GETRAENKE_ANTIALK = "Getränke (antialkoholisch)";
const SPEISEN = "Speisen";
const SONSTIGES = "Sonstiges";

export default {
    GETRAENKE_ALK: GETRAENKE_ALK,
    GETRAENKE_ANTIALK: GETRAENKE_ANTIALK,
    SPEISEN: SPEISEN,
    SONSTIGES: SONSTIGES
}